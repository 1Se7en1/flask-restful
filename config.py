# -*- coding: utf-8 -*-
import os
import multiprocessing
from flask_sqlalchemy import SQLAlchemy

MODE = 'develop'  # develop: 开发模式; production: 生产模式


class ProductionConfig(object):
    """
    生产配置
    """
    SECRET_KEY = "demo replace this key"
    BIND = '127.0.0.1:5000'
    WORKERS = multiprocessing.cpu_count() * 2 + 1
    WORKER_CONNECTIONS = 10000
    BACKLOG = 64
    TIMEOUT = 60
    LOG_LEVEL = 'INFO'
    LOG_DIR_PATH = os.path.join(os.path.dirname(__file__), 'logs')
    LOG_FILE_MAX_BYTES = 1024 * 1024 * 100
    LOG_FILE_BACKUP_COUNT = 10
    PID_FILE = 'run.pid'
    POSTGRES_HOST = os.environ.get('POSTGRES_HOST', 'localhost')
    POSTGRES_PORT = os.environ.get('POSTGRES_PORT', '3306')
    POSTGRES_USER = os.environ.get('POSTGRES_USER', 'root')
    POSTGRES_PASS = os.environ.get('POSTGRES_PASS', '12345678')
    POSTGRES_DB = os.environ.get('POSTGRES_DB', 'example')
    POSTGRES_CHARSET = os.environ.get('POSTGRES_CHARSET', 'utf8mb4')
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://%s:%s@%s:%s/%s?charset=%s' % (
        POSTGRES_USER,
        POSTGRES_PASS,
        POSTGRES_HOST,
        POSTGRES_PORT,
        POSTGRES_DB,
        POSTGRES_CHARSET
    )
    # SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(os.path.dirname(__file__), 'example.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    REDIS_EXPIRE = 60*10
    REDIS_HOST = 'localhost'
    REDIS_PORT = 6379
    REDIS_DB = 0


class DevelopConfig(object):
    """
    开发配置
    """
    SECRET_KEY = "demo replace this key"
    BIND = '0.0.0.0:5000'
    WORKERS = 2
    WORKER_CONNECTIONS = 1000
    BACKLOG = 64
    TIMEOUT = 30
    LOG_LEVEL = 'DEBUG'
    LOG_DIR_PATH = os.path.join(os.path.dirname(__file__), 'logs')
    LOG_FILE_MAX_BYTES = 1024 * 1024
    LOG_FILE_BACKUP_COUNT = 1
    PID_FILE = 'run.pid'
    POSTGRES_HOST = os.environ.get('POSTGRES_HOST', 'localhost')
    POSTGRES_PORT = os.environ.get('POSTGRES_PORT', '3306')
    POSTGRES_USER = os.environ.get('POSTGRES_USER', 'root')
    POSTGRES_PASS = os.environ.get('POSTGRES_PASS', '12345678')
    POSTGRES_DB = os.environ.get('POSTGRES_DB', 'example')
    POSTGRES_CHARSET = os.environ.get('POSTGRES_CHARSET', 'utf8mb4')
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://%s:%s@%s:%s/%s?charset=%s' % (
        POSTGRES_USER,
        POSTGRES_PASS,
        POSTGRES_HOST,
        POSTGRES_PORT,
        POSTGRES_DB,
        POSTGRES_CHARSET
    )
    # SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(os.path.dirname(__file__), 'example.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    REDIS_EXPIRE = 60*10
    REDIS_HOST = 'localhost'
    REDIS_PORT = 6379
    REDIS_DB = 0


if MODE == 'production':
    config = ProductionConfig
else:
    config = DevelopConfig
