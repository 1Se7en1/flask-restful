# -*- coding: utf-8 -*-

import string
from flask_babel import Babel
from hashids import Hashids
from flask_login import LoginManager

babel = Babel()

hash_ids = Hashids(salt='hvwptlmj129d5quf', min_length=8, alphabet=string.ascii_lowercase + string.digits)

login_manager = LoginManager()
