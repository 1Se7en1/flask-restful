# -*- coding: utf-8 -*-
from .database import db
from sqlalchemy import Column, Integer, String, Text, ForeignKey, DateTime, UniqueConstraint, Index
from .base import BaseModel
from typing import Optional

class ProfilesModel(db.Model, BaseModel):
    """
    示例模型类
    """
    __tablename__ = 'profiles'
    nickname = Column(String(32), index=True)
    signature = Column(String(32))