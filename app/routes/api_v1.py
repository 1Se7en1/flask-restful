# -*- coding: utf-8 -*-
from flask import Blueprint
from flask_restful import Api
from app.resources import profiles
from app.resources import userlist

api_v1 = Blueprint('api_v1', __name__)
api = Api(api_v1)

api.add_resource(profiles.ProfileListResource, '/profiles')
api.add_resource(profiles.ProfileResource, '/profiles/<string:id>')
api.add_resource(userlist.UserResource, '/user/hello')