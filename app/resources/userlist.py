# -*- coding: utf-8 -*-
from flask import current_app, abort
from flask_restful import Resource
from flask_restful.reqparse import RequestParser
from sqlalchemy.exc import SQLAlchemyError
from flasky import hash_ids
from app.models import db
from app.common import code, pretty_result
from app.models.user import UserModel

class UserResource(Resource):

    def __init__(self):
        self.parser = RequestParser()

    def get(self):
        self.parser.add_argument("name", type=int, location="args")
        param = self.parser.parse_args()
        return pretty_result(code.OK, data=param)