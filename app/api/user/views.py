
from ..user import user
from flask import current_app,jsonify,flash
from app.models import UserModel,db
from app.models.user import verify_password
from app.common import code, pretty_result,authorized_sign_required
from app.common.redis import Redis
from flask_login import login_user, login_required, current_user, logout_user
import demjson

from flask import Flask, abort, request, jsonify, g, url_for
from app.models.user import auth

@user.route('/test', methods=['GET', 'POST'])
def test():
    username = request.args.get('username')
    user = UserModel(username='丽丽',age='29')
    # db.session.add(user)
    # db.session.commit()
    user = UserModel.query.filter_by(username=username).first()
    data = {
                'page_num': '1',
                'page_size': '3',
                'username':user.id
            }
    return  pretty_result(code=code.OK,msg="success",data=data)

@user.context_processor
def inject_user():
    user = UserModel.query.first()
    return dict(user=user)

@user.route('/users/register', methods=['POST'])
def register():
    username = request.args.get('username')
    password = request.args.get('password')
    if username is None or password is None:
        msg = '用户名不能为空' if username is None else '密码不能为空'
        return  pretty_result(code = code.PARAM_ERROR,msg=msg,data={})
    if UserModel.query.filter_by(username=username).first() is not None:
        return  pretty_result(code = code.PARAM_ERROR,msg='不能重复注册',data={})
    user = UserModel(username=username)
    user.hash_password(password)
    db.session.add(user)
    db.session.commit()
    return  pretty_result(code = code.OK,data={'username': user.username,'uid':user.id})

@user.route('/users/login', methods=['POST', 'GET'])
# @authorized_sign_required
def login():
    username = request.args.get('username')
    password = request.args.get('password')
    user = UserModel.query.filter_by(username=username).first()
    result = verify_password(username_or_token=username,password=password)
    if user and result :
        # if current_user.is_authenticated:
        #     logout_user()
        #     print('User Logout')
        # else:
        #     login_user(user,True)
        #     print('User Login')
        login_user(user,True)
        return  pretty_result(code = code.OK,data={'username': user.username,'uid':user.id})
    else:
        msg = code.CODE_MSG_MAP[code.LOGIN_ERROR_PWD]
        return pretty_result(code = code.LOGIN_ERROR_PWD,msg = msg,data={})

@user.route('/users/logout', methods=['POST'])
@login_required
def logout():
    logout_user()
    return  pretty_result(code = code.OK,msg= 'Success',data={})

@user.route('/users/<int:id>', methods=['POST','GET'])
def get_user(id):
    user = UserModel.query.get(id)
    if not user:
        abort(400)
    return  pretty_result(code = code.OK,data={'username': user.username})

@user.route('/users/token', methods=['POST','GET'])
@auth.login_required
def get_auth_token():
    expires_in = 3600*24*7
    token = g.user.generate_auth_token(expires_in)
    return pretty_result(code = code.OK,data={'token': token.decode('ascii'),'duration': expires_in})

@user.route('/users/resource', methods=['POST','GET'])
@auth.login_required
def get_resource():
    return pretty_result(code = code.OK,data={'content': 'Hello, %s!' % g.user.username})